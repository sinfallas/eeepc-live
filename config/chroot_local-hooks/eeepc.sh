#!/bin/sh
# Demo a few features that are not turned on by default
SCRIPTS_DEFAULTS=/etc/default/eeepc-acpi-scripts
sed -i "s/ENABLE_OSD='no'/ENABLE_OSD='yes'/" $SCRIPTS_DEFAULTS
# In LXDE, volume keys have no default bindings, so make use of the
# eeepc-acpi-scripts handlers.
sed -i "s/FnF_MUTE='NONE'/FnF_MUTE='handle_mute_toggle'/" $SCRIPTS_DEFAULTS
sed -i "s/FnF_VOLUMEDOWN='NONE'/FnF_VOLUMEDOWN='handle_volume_down'/" $SCRIPTS_DEFAULTS
sed -i "s/FnF_VOLUMEUP='NONE'/FnF_VOLUMEUP='handle_volume_up'/" $SCRIPTS_DEFAULTS
sed -i "s/LOCK_SCREEN_ON_SUSPEND='yes'/LOCK_SCREEN_ON_SUSPEND='no'/" $SCRIPTS_DEFAULTS
echo "vf=screenshot" >> /etc/mplayer/mplayer.conf

# Include some doc (iceweasel home page)
mkdir -p /usr/local/share/eeepc-live
(cd /usr/local/share/eeepc-live ;
 wget -E -H -k -K -p -e robots=off http://wiki.debian.org/DebianEeePC/Live/UsersGuide)

